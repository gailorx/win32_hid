pub use win32_types::{LONG, ULONG, PULONG,
					SHORT, USHORT, PUSHORT,
					CHAR, UCHAR, PCHAR,
					BYTE, BOOLEAN};

pub type USAGE = USHORT;
pub type PUSAGE = *mut USHORT;

pub type NTSTATUS = LONG;