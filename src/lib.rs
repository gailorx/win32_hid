#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

extern crate win32_types;

use types::{USAGE, PUSAGE, NTSTATUS, LONG, ULONG, PULONG, UCHAR, PCHAR, USHORT, PUSHORT, BOOLEAN};

pub mod types;

pub fn NT_SUCCESS(Status: NTSTATUS) -> bool { Status >= 0 }
pub fn NT_INFORMATION(Status: NTSTATUS) -> bool { (Status as ULONG >> 30) == 1 }
pub fn NT_WARNING(Status: NTSTATUS) -> bool { (Status as ULONG >> 30) == 2 }
pub fn NT_ERROR(Status: NTSTATUS) -> bool { (Status as ULONG >> 30) == 3 }

#[repr(C)]
#[allow(missing_copy_implementations)]
pub struct _HIDP_PREPARSED_DATA;
pub type PHIDP_PREPARSED_DATA = *mut _HIDP_PREPARSED_DATA;

pub mod HID_USAGE_PAGE {
	use types::USAGE;

	pub const UNDEFINED: USAGE = 0x00;
	pub const GENERIC: USAGE = 0x01;
	pub const SIMULATION: USAGE = 0x02;
	pub const VR: USAGE = 0x03;
	pub const SPORT: USAGE = 0x04;
	pub const GAME: USAGE = 0x05;
	pub const KEYBOARD: USAGE = 0x07;
	pub const LED: USAGE = 0x08;
	pub const BUTTON: USAGE = 0x09;
	pub const ORDINAL: USAGE = 0x0A;
	pub const TELEPHONY: USAGE = 0x0B;
	pub const CONSUMER: USAGE = 0x0C;
	pub const DIGITIZER: USAGE = 0x0D;
	pub const UNICODE: USAGE = 0x10;
	pub const ALPHANUMERIC: USAGE = 0x14;
	pub const SENSOR: USAGE = 0x20;
	pub const BARCODE_SCANNER: USAGE = 0x8C;
	pub const WEIGHING_DEVICE: USAGE = 0x8D;
	pub const MAGNETIC_STRIPE_READER: USAGE = 0x8E;
	pub const MICROSOFT_BLUETOOTH_HANDSFREE: USAGE = 0xFFF3;
	pub const VENDOR_DEFINED_BEGIN: USAGE = 0xFF00;
	pub const VENDOR_DEFINED_END: USAGE = 0xFFFF;
}

// Generic Desktop Page (0x01;
pub mod HID_USAGE_GENERIC {
	use types::USAGE;

	pub const POINTER: USAGE = 0x01;
	pub const MOUSE: USAGE = 0x02;
	pub const JOYSTICK: USAGE = 0x04;
	pub const GAMEPAD: USAGE = 0x05;
	pub const KEYBOARD: USAGE = 0x06;
	pub const KEYPAD: USAGE = 0x07;
	pub const SYSTEM_CTL: USAGE = 0x80;

	pub const X: USAGE = 0x30;
	pub const Y: USAGE = 0x31;
	pub const Z: USAGE = 0x32;
	pub const RX: USAGE = 0x33;
	pub const RY: USAGE = 0x34;
	pub const RZ: USAGE = 0x35;
	pub const SLIDER: USAGE = 0x36;
	pub const DIAL: USAGE = 0x37;
	pub const WHEEL: USAGE = 0x38;
	pub const HATSWITCH: USAGE = 0x39;
	pub const COUNTED_BUFFER: USAGE = 0x3A;
	pub const BYTE_COUNT: USAGE = 0x3B;
	pub const MOTION_WAKEUP: USAGE = 0x3C;
	pub const VX: USAGE = 0x40;
	pub const VY: USAGE = 0x41;
	pub const VZ: USAGE = 0x42;
	pub const VBRX: USAGE = 0x43;
	pub const VBRY: USAGE = 0x44;
	pub const VBRZ: USAGE = 0x45;
	pub const VNO: USAGE = 0x46;
	pub const RESOLUTION_MULTIPLIER: USAGE = 0x48;
	pub const SYSCTL_POWER: USAGE = 0x81;
	pub const SYSCTL_SLEEP: USAGE = 0x82;
	pub const SYSCTL_WAKE: USAGE = 0x83;
	pub const SYSCTL_CONTEXT_MENU: USAGE = 0x84;
	pub const SYSCTL_MAIN_MENU: USAGE = 0x85;
	pub const SYSCTL_APP_MENU: USAGE = 0x86;
	pub const SYSCTL_HELP_MENU: USAGE = 0x87;
	pub const SYSCTL_MENU_EXIT: USAGE = 0x88;
	pub const SYSCTL_MENU_SELECT: USAGE = 0x89;
	pub const SYSCTL_MENU_RIGHT: USAGE = 0x8A;
	pub const SYSCTL_MENU_LEFT: USAGE = 0x8B;
	pub const SYSCTL_MENU_UP: USAGE = 0x8C;
	pub const SYSCTL_MENU_DOWN: USAGE = 0x8D;
}

// Simulation Controls Page (0x02)
pub mod HID_USAGE_SIMULATION {
	use types::USAGE;
	
	pub const RUDDER: USAGE = 0xBA;
	pub const THROTTLE: USAGE = 0xBB;
}

// Keyboard/Keypad Page (0x07)
pub mod HID_USAGE_KEYBOARD {
	use types::USAGE;

	// Error "keys"
	pub const NOEVENT: USAGE = 0x00;
	pub const ROLLOVER: USAGE = 0x01;
	pub const POSTFAIL: USAGE = 0x02;
	pub const UNDEFINED: USAGE = 0x03;

	// Letters
	pub const aA: USAGE = 0x04;
	pub const zZ: USAGE = 0x1D;

	// Numbers
	pub const ONE: USAGE = 0x1E;
	pub const ZERO: USAGE = 0x27;

	// Modifier Keys
	pub const LCTRL: USAGE = 0xE0;
	pub const LSHFT: USAGE = 0xE1;
	pub const LALT: USAGE = 0xE2;
	pub const LGUI: USAGE = 0xE3;
	pub const RCTRL: USAGE = 0xE4;
	pub const RSHFT: USAGE = 0xE5;
	pub const RALT: USAGE = 0xE6;
	pub const RGUI: USAGE = 0xE7;
	pub const SCROLL_LOCK: USAGE = 0x47;
	pub const NUM_LOCK: USAGE = 0x53;
	pub const CAPS_LOCK: USAGE = 0x39;

	// Function keys
	pub const F1: USAGE = 0x3A;
	pub const F12: USAGE = 0x45;

	pub const RETURN: USAGE = 0x28;
	pub const ESCAPE: USAGE = 0x29;
	pub const DELETE: USAGE = 0x2A;

	pub const PRINT_SCREEN: USAGE = 0x46;
}

// LED Page (0x08;
pub mod HID_USAGE_LED {
	use types::USAGE;

	pub const NUM_LOCK: USAGE = 0x01;
	pub const CAPS_LOCK: USAGE = 0x02;
	pub const SCROLL_LOCK: USAGE = 0x03;
	pub const COMPOSE: USAGE = 0x04;
	pub const KANA: USAGE = 0x05;
	pub const POWER: USAGE = 0x06;
	pub const SHIFT: USAGE = 0x07;
	pub const DO_NOT_DISTURB: USAGE = 0x08;
	pub const MUTE: USAGE = 0x09;
	pub const TONE_ENABLE: USAGE = 0x0A;
	pub const HIGH_CUT_FILTER: USAGE = 0x0B;
	pub const LOW_CUT_FILTER: USAGE = 0x0C;
	pub const EQUALIZER_ENABLE: USAGE = 0x0D;
	pub const SOUND_FIELD_ON: USAGE = 0x0E;
	pub const SURROUND_FIELD_ON: USAGE = 0x0F;
	pub const REPEAT: USAGE = 0x10;
	pub const STEREO: USAGE = 0x11;
	pub const SAMPLING_RATE_DETECT: USAGE = 0x12;
	pub const SPINNING: USAGE = 0x13;
	pub const CAV: USAGE = 0x14;
	pub const CLV: USAGE = 0x15;
	pub const RECORDING_FORMAT_DET: USAGE = 0x16;
	pub const OFF_HOOK: USAGE = 0x17;
	pub const RING: USAGE = 0x18;
	pub const MESSAGE_WAITING: USAGE = 0x19;
	pub const DATA_MODE: USAGE = 0x1A;
	pub const BATTERY_OPERATION: USAGE = 0x1B;
	pub const BATTERY_OK: USAGE = 0x1C;
	pub const BATTERY_LOW: USAGE = 0x1D;
	pub const SPEAKER: USAGE = 0x1E;
	pub const HEAD_SET: USAGE = 0x1F;
	pub const HOLD: USAGE = 0x20;
	pub const MICROPHONE: USAGE = 0x21;
	pub const COVERAGE: USAGE = 0x22;
	pub const NIGHT_MODE: USAGE = 0x23;
	pub const SEND_CALLS: USAGE = 0x24;
	pub const CALL_PICKUP: USAGE = 0x25;
	pub const CONFERENCE: USAGE = 0x26;
	pub const STAND_BY: USAGE = 0x27;
	pub const CAMERA_ON: USAGE = 0x28;
	pub const CAMERA_OFF: USAGE = 0x29;
	pub const ON_LINE: USAGE = 0x2A;
	pub const OFF_LINE: USAGE = 0x2B;
	pub const BUSY: USAGE = 0x2C;
	pub const READY: USAGE = 0x2D;
	pub const PAPER_OUT: USAGE = 0x2E;
	pub const PAPER_JAM: USAGE = 0x2F;
	pub const REMOTE: USAGE = 0x30;
	pub const FORWARD: USAGE = 0x31;
	pub const REVERSE: USAGE = 0x32;
	pub const STOP: USAGE = 0x33;
	pub const REWIND: USAGE = 0x34;
	pub const FAST_FORWARD: USAGE = 0x35;
	pub const PLAY: USAGE = 0x36;
	pub const PAUSE: USAGE = 0x37;
	pub const RECORD: USAGE = 0x38;
	pub const ERROR: USAGE = 0x39;
	pub const SELECTED_INDICATOR: USAGE = 0x3A;
	pub const IN_USE_INDICATOR: USAGE = 0x3B;
	pub const MULTI_MODE_INDICATOR: USAGE = 0x3C;
	pub const INDICATOR_ON: USAGE = 0x3D;
	pub const INDICATOR_FLASH: USAGE = 0x3E;
	pub const INDICATOR_SLOW_BLINK: USAGE = 0x3F;
	pub const INDICATOR_FAST_BLINK: USAGE = 0x40;
	pub const INDICATOR_OFF: USAGE = 0x41;
	pub const FLASH_ON_TIME: USAGE = 0x42;
	pub const SLOW_BLINK_ON_TIME: USAGE = 0x43;
	pub const SLOW_BLINK_OFF_TIME: USAGE = 0x44;
	pub const FAST_BLINK_ON_TIME: USAGE = 0x45;
	pub const FAST_BLINK_OFF_TIME: USAGE = 0x46;
	pub const INDICATOR_COLOR: USAGE = 0x47;
	pub const RED: USAGE = 0x48;
	pub const GREEN: USAGE = 0x49;
	pub const AMBER: USAGE = 0x4A;
	pub const GENERIC_INDICATOR: USAGE = 0x4B;
}


//  Telephony Device Page (0x0B;
pub mod HID_USAGE_TELEPHONY {
	use types::USAGE;

	pub const PHONE: USAGE = 0x01;
	pub const ANSWERING_MACHINE: USAGE = 0x02;
	pub const MESSAGE_CONTROLS: USAGE = 0x03;
	pub const HANDSET: USAGE = 0x04;
	pub const HEADSET: USAGE = 0x05;
	pub const KEYPAD: USAGE = 0x06;
	pub const PROGRAMMABLE_BUTTON: USAGE = 0x07;
	pub const REDIAL: USAGE = 0x24;
	pub const TRANSFER: USAGE = 0x25;
	pub const DROP: USAGE = 0x26;
	pub const LINE: USAGE = 0x2A;
	pub const RING_ENABLE: USAGE = 0x2D;
	pub const SEND: USAGE = 0x31;
	pub const KEYPAD_0: USAGE = 0xB0;
	pub const KEYPAD_D: USAGE = 0xBF;
	pub const HOST_AVAILABLE: USAGE = 0xF1;
}

// Consumer Controls Page (0x0C)
pub mod HID_USAGE_CONSUMER {
	use types::USAGE;

	pub const CTRL: USAGE = 0x01;

	// channel
	pub const CHANNEL_INCREMENT: USAGE = 0x9C;
	pub const CHANNEL_DECREMENT: USAGE = 0x9D;

	// transport control
	pub const PLAY: USAGE = 0xB0;
	pub const PAUSE: USAGE = 0xB1;
	pub const RECORD: USAGE = 0xB2;
	pub const FAST_FORWARD: USAGE = 0xB3;
	pub const REWIND: USAGE = 0xB4;
	pub const SCAN_NEXT_TRACK: USAGE = 0xB5;
	pub const SCAN_PREV_TRACK: USAGE = 0xB6;
	pub const STOP: USAGE = 0xB7;
	pub const PLAY_PAUSE: USAGE = 0xCD;

	// audio
	pub const VOLUME: USAGE = 0xE0;
	pub const BALANCE: USAGE = 0xE1;
	pub const MUTE: USAGE = 0xE2;
	pub const BASS: USAGE = 0xE3;
	pub const TREBLE: USAGE = 0xE4;
	pub const BASS_BOOST: USAGE = 0xE5;
	pub const SURROUND_MODE: USAGE = 0xE6;
	pub const LOUDNESS: USAGE = 0xE7;
	pub const MPX: USAGE = 0xE8;
	pub const VOLUME_INCREMENT: USAGE = 0xE9;
	pub const VOLUME_DECREMENT: USAGE = 0xEA;

	// supplementary audio
	pub const BASS_INCREMENT: USAGE = 0x152;
	pub const BASS_DECREMENT: USAGE = 0x153;
	pub const TREBLE_INCREMENT: USAGE = 0x154;
	pub const TREBLE_DECREMENT: USAGE = 0x155;

	// Application Launch
	pub const AL_CONFIGURATION: USAGE = 0x183;
	pub const AL_EMAIL: USAGE = 0x18A;
	pub const AL_CALCULATOR: USAGE = 0x192;
	pub const AL_BROWSER: USAGE = 0x194;

	// Application Control
	pub const AC_SEARCH: USAGE = 0x221;
	pub const AC_GOTO: USAGE = 0x222;
	pub const AC_HOME: USAGE = 0x223;
	pub const AC_BACK: USAGE = 0x224;
	pub const AC_FORWARD: USAGE = 0x225;
	pub const AC_STOP: USAGE = 0x226;
	pub const AC_REFRESH: USAGE = 0x227;
	pub const AC_PREVIOUS: USAGE = 0x228;
	pub const AC_NEXT: USAGE = 0x229;
	pub const AC_BOOKMARKS: USAGE = 0x22A;
	pub const AC_PAN: USAGE = 0x238;
}

// Digitizer Page (0x0D;
pub mod HID_USAGE_DIGITIZER {
	use types::USAGE;

	pub const PEN: USAGE = 0x02;
	pub const IN_RANGE: USAGE = 0x32;
	pub const TIP_SWITCH: USAGE = 0x42;
	pub const BARREL_SWITCH: USAGE = 0x44;
}


// Microsoft Bluetooth Handsfree Page (0xFFF3;
pub mod HID_USAGE_MS_BTH_HF {
	use types::USAGE;

	pub const DIALNUMBER: USAGE = 0x21;
	pub const DIALMEMORY: USAGE = 0x22;
}

#[repr(C)]
pub struct HIDP_CAPS {
    pub Usage: USAGE,
    pub UsagePage: USAGE,
    pub InputReportByteLength: USHORT,
    pub OutputReportByteLength: USHORT,
    pub FeatureReportByteLength: USHORT,
    Reserved: [USHORT; 17],

    pub NumberLinkCollectionNodes: USHORT,

    pub NumberInputButtonCaps: USHORT,
    pub NumberInputValueCaps: USHORT,
    pub NumberInputDataIndices: USHORT,

    pub NumberOutputButtonCaps: USHORT,
    pub NumberOutputValueCaps: USHORT,
    pub NumberOutputDataIndices: USHORT,

    pub NumberFeatureButtonCaps: USHORT,
    pub NumberFeatureValueCaps: USHORT,
    pub NumberFeatureDataIndices: USHORT,
}
impl Copy for HIDP_CAPS {}
pub type PHIDP_CAPS = *mut HIDP_CAPS;


#[repr(C)]
pub struct ANON_NOT_RANGE_STRUCT {
    pub Usage: USAGE,
    Reserved1: USHORT,
    pub StringIndex: USHORT,
    Reserved2: USHORT,
    pub DesignatorIndex: USHORT,
    Reserved3: USHORT,
    pub DataIndex: USHORT,
    Reserved4: USHORT,
}
impl Copy for ANON_NOT_RANGE_STRUCT {}

#[repr(C)]
pub struct ANON_RANGE_STRUCT {
    pub UsageMin: USAGE,
    pub UsageMax: USAGE,
    pub StringMin: USHORT,
    pub StringMax: USHORT,
    pub DesignatorMin: USHORT,
    pub DesignatorMax: USHORT,
    pub DataIndexMin: USHORT,
    pub DataIndexMax: USHORT,
}
impl Copy for ANON_RANGE_STRUCT {}

#[repr(C)]
pub struct HIDP_BUTTON_CAPS {
    pub UsagePage: USAGE,
    pub ReportID: UCHAR,
    pub IsAlias: BOOLEAN,

    pub BitField: USHORT,
    pub LinkCollection: USHORT,   // A unique internal index pointer

    pub LinkUsage: USAGE,
    pub LinkUsagePage: USAGE,

    pub IsRange: BOOLEAN,
    pub IsStringRange: BOOLEAN,
    pub IsDesignatorRange: BOOLEAN,
    pub IsAbsolute: BOOLEAN,

    Reserved: [UCHAR; 10],
    RANGE_UNION_MEMBER_DATA: [u8; 16],
}
impl Copy for HIDP_BUTTON_CAPS {}
impl HIDP_BUTTON_CAPS {
	pub fn Range(&self) -> &ANON_RANGE_STRUCT {
		unsafe { &(*(self.RANGE_UNION_MEMBER_DATA.as_ptr() as *const ANON_RANGE_STRUCT)) }
	}

	pub fn NotRange(&self) -> &ANON_NOT_RANGE_STRUCT {
		unsafe { &(*(self.RANGE_UNION_MEMBER_DATA.as_ptr() as *const ANON_NOT_RANGE_STRUCT)) }
	}

	pub fn Range_mut(&self) -> &ANON_RANGE_STRUCT {
		unsafe { &(*(self.RANGE_UNION_MEMBER_DATA.as_ptr() as *mut ANON_RANGE_STRUCT)) }
	}

	pub fn NotRange_mut(&self) -> &ANON_NOT_RANGE_STRUCT {
		unsafe { &(*(self.RANGE_UNION_MEMBER_DATA.as_ptr() as *mut ANON_NOT_RANGE_STRUCT)) }
	}
}
pub type PHIDP_BUTTON_CAPS = *mut HIDP_BUTTON_CAPS;

#[repr(C)]
pub struct HIDP_VALUE_CAPS {
    pub UsagePage: USAGE,
    pub ReportID: UCHAR,
    pub IsAlias: BOOLEAN,

    pub BitField: USHORT,
    pub LinkCollection: USHORT,   // A unique internal index pointer

    pub LinkUsage: USAGE,
    pub LinkUsagePage: USAGE,

    pub IsRange: BOOLEAN,
    pub IsStringRange: BOOLEAN,
    pub IsDesignatorRange: BOOLEAN,
    pub IsAbsolute: BOOLEAN,

    pub HasNull: BOOLEAN,        // Does this channel have a null report   union
    Reserved: UCHAR,
    pub BitSize: USHORT,        // How many bits are devoted to this value?

    pub ReportCount: USHORT,    // See Note below.  Usually set to 1.
    Reserved2: [USHORT; 5],

    pub UnitsExp: ULONG,
    pub Units: ULONG,

    pub LogicalMin: LONG,
    pub LogicalMax: LONG,
    pub PhysicalMin: LONG,
    pub PhysicalMax: LONG,

    RANGE_UNION_MEMBER_DATA: [u8; 16],
}
impl Copy for HIDP_VALUE_CAPS {}
impl HIDP_VALUE_CAPS {
	pub fn Range(&self) -> &ANON_RANGE_STRUCT {
		unsafe { &(*(self.RANGE_UNION_MEMBER_DATA.as_ptr() as *const ANON_RANGE_STRUCT)) }
	}

	pub fn NotRange(&self) -> &ANON_NOT_RANGE_STRUCT {
		unsafe { &(*(self.RANGE_UNION_MEMBER_DATA.as_ptr() as *const ANON_NOT_RANGE_STRUCT)) }
	}
}
pub type PHIDP_VALUE_CAPS = *mut HIDP_VALUE_CAPS;

#[repr(C)]
pub enum HIDP_REPORT_TYPE { 
  HidP_Input,
  HidP_Output,
  HidP_Feature
}
impl Copy for HIDP_REPORT_TYPE {}

#[link(name = "hid")]
extern "stdcall" {
	pub fn HidP_GetCaps(PreparsedData: PHIDP_PREPARSED_DATA, Capabilities: PHIDP_CAPS) -> NTSTATUS;
	pub fn HidP_GetButtonCaps(ReportType: HIDP_REPORT_TYPE, ButtonCaps: PHIDP_BUTTON_CAPS, ButtonCapsLength: PUSHORT, PreparsedData: PHIDP_PREPARSED_DATA) -> NTSTATUS;
	pub fn HidP_GetValueCaps(ReportType: HIDP_REPORT_TYPE, ValueCaps: PHIDP_VALUE_CAPS, ValueCapsLength: PUSHORT, PreparsedData: PHIDP_PREPARSED_DATA) -> NTSTATUS;
	pub fn HidP_GetUsages(ReportType: HIDP_REPORT_TYPE, UsagePage: USAGE, LinkCollection: USHORT, UsageList: PUSAGE, UsageLength: PULONG, PreparsedData: PHIDP_PREPARSED_DATA, Report: PCHAR, ReportLength: ULONG) -> NTSTATUS;
	pub fn HidP_GetUsageValue(ReportType: HIDP_REPORT_TYPE, UsagePage: USAGE, LinkCollection: USHORT, Usage: USAGE, UsageValue: PULONG, PreparsedData: PHIDP_PREPARSED_DATA, Report: PCHAR, ReportLength: ULONG) -> NTSTATUS;
}